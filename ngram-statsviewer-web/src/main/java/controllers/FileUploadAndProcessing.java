package controllers;

import databaseServicesApi.DatabaseAdderInterface;
import libngram.PhraseDataHolder;
import libngram.PhrasesWithWordsAndNgramsFromFileExtractor;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

@Controller
@RequestMapping("/upload")
public class FileUploadAndProcessing {
    private static final Logger logger = LoggerFactory.getLogger(FileUploadAndProcessing.class);

    private class StatsCollectionTask implements Runnable {
        private final InputStream inputStream;
        private final int ngramLength;

        public StatsCollectionTask(InputStream inputStream, int ngramLength) {
            this.inputStream = inputStream;
            this.ngramLength = ngramLength;
        }

        public void run() {
            try {
                PhrasesWithWordsAndNgramsFromFileExtractor extractor =
                        new PhrasesWithWordsAndNgramsFromFileExtractor(inputStream, ngramLength);
                dbAdder.clearDatabase();

                long startTime = System.currentTimeMillis();
                for (PhraseDataHolder data : extractor) {
                    dbAdder.addToDatabase(data);
                }
                long endTime = System.currentTimeMillis();
                long totalRunningTime = (endTime - startTime) / 1000;
                logger.debug("File processing success!");
                logger.debug("Finished in " + totalRunningTime + " sec.");
            } catch (Throwable throwable) {
                logger.debug("Unexpected error occurred.", throwable);
            }
        }
    }

    @Autowired
    private DatabaseAdderInterface dbAdder;

    @RequestMapping(method = RequestMethod.GET)
    public String getUploadPage() {
        logger.info("Get file upload page.");
        return "uploader-processing/upload";
    }

    private boolean fileContentTypeIsValid(String fileContentType) {
        return fileContentType.equals(MediaType.TEXT_PLAIN_VALUE);
    }

    @RequestMapping(method = RequestMethod.POST)
    public String uploadFileForProcessing(Model model, MultipartHttpServletRequest request) {
        logger.info("Uploading and processing a file.");

        int ngramLength;

        try {
            ngramLength = Integer.parseInt(request.getParameter("ngramLength")); //TODO dirty hack, make as requestparam, otherwise 404 is throwing when empty field
        } catch (NumberFormatException e) {
            model.addAttribute("errorFileProcessingMessage", "specify ngram length.");
            return "uploader-processing/error";
        }

        if (ngramLength <= 0) {
            model.addAttribute("errorFileProcessingMessage", "specify ngram length higher as 1 or greater.");
            return "uploader-processing/error";
        }

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            Iterator<String> itr = request.getFileNames();
            MultipartFile file;

            while (itr.hasNext()) {
                file = request.getFile(itr.next());
                if (!file.isEmpty() && fileContentTypeIsValid(file.getContentType())) {
                    try {
                        Thread statsCollection = new Thread(new StatsCollectionTask(file.getInputStream(), ngramLength));
                        statsCollection.start();
                    } catch (IOException e) {
                        logger.info("Exception during file processing occured.", e);
                    } catch (MaxUploadSizeExceededException e) {
                        logger.info("File size is over maximum allowed.");
                        model.addAttribute("errorFileProcessingMessage", "upload file smaller that 2 MB.");
                        return "uploader-processing/error";
                    }
                } else {
                    logger.info("Wrong MIME type! Need text/plain, type found: " + file.getContentType());
                    model.addAttribute("errorFileProcessingMessage", "upload file with correct extension (only *.txt or any text/plain files supported).");
                    return "uploader-processing/error";
                }
            }
        }
        return "uploader-processing/in-progress";
    }
}
