package controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeAndNavigationController {
    private static final Logger logger = LoggerFactory.getLogger(HomeAndNavigationController.class);

    @RequestMapping(method = RequestMethod.GET)
    public String redirectToHomepage() {
        logger.info("Redirecting to ngrams view page.");
        return "redirect:/ngrams/view";
    }

    @RequestMapping(value = "about", method = RequestMethod.GET)
    public String getAboutPage() {
        logger.info("Viewing about page.");
        return "about";
    }
}
