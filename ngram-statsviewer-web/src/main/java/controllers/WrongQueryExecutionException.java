package controllers;

public class WrongQueryExecutionException extends Throwable {
    public WrongQueryExecutionException(String message) {
        super(message);
    }
}
