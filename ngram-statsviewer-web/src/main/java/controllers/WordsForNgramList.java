package controllers;

import databaseServicesApi.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/ngram")
public class WordsForNgramList {
    private static final String COLLECTION_URL_SERVLET_IS_MAPPED = "/ngram";
    private static final String REGEX_PATTERN_TO_EXTRACT_ALL_WORDS = ".";
    private static final int AMOUNT_OF_ELEMENTS_PER_PAGE = 10;
    private static final Logger logger = LoggerFactory.getLogger(WordsForNgramList.class);

    private static final String SEARCH_PLACEHOLDER = "Search among all words for this ngram (partial/regex)";

    @Value("#{wordsDatabaseReader}")
    WordsDatabaseReaderInterface dbreader;

    private void GetItemsTable(Model model, HttpServletRequest request, final String patternToFilter, final boolean isSearchResult) throws WrongQueryExecutionException {
        String ngramData = null;
        int pageId = 1;
        WordsSortFieldName wordsSortFieldName = null;
        Order order = null;
        Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if (pathVariables != null) {
            if (pathVariables.containsKey("ngram")) {
                ngramData = pathVariables.get("ngram").toString();
            }
            if (pathVariables.containsKey("pageId")) {
                pageId = NumberUtils.toInt("" + pathVariables.get("pageId"), pageId);
            }
            if (pathVariables.containsKey("wordsSortFieldName")) {
                wordsSortFieldName = WordsSortFieldName.valueOf(pathVariables.get("wordsSortFieldName").toString());
            }
            if (pathVariables.containsKey("order")) {
                order = Order.valueOf(pathVariables.get("order").toString());
            }
        }

        int amountOfPages = (int) Math.ceil((double) dbreader.getAmountOfWordsWithCountersInDatabase(ngramData, patternToFilter) / AMOUNT_OF_ELEMENTS_PER_PAGE);
        // In the case if input page is wrong or even negative, first page id will be applied by Rest Parser
        if (pageId > amountOfPages) {
            pageId = amountOfPages;
        }

        List<WordForNgramWithCountDto> listOfWordsForNgram;
        String pathPostConditions = "";

        try {
            listOfWordsForNgram = dbreader.getListOfWordsWithCounters(
                    new WordsListQueryObject.WordsListQueryBuilder(ngramData, AMOUNT_OF_ELEMENTS_PER_PAGE, pageId - 1, patternToFilter)
                            .orderByField(wordsSortFieldName)
                            .order(order).build()
            );

        } catch (QueryObjectBuilderValidationError e) {
            try {
                listOfWordsForNgram = dbreader.getListOfWordsWithCounters(
                        new WordsListQueryObject.WordsListQueryBuilder(ngramData, AMOUNT_OF_ELEMENTS_PER_PAGE, 0, REGEX_PATTERN_TO_EXTRACT_ALL_WORDS).build()
                );
            } catch (QueryObjectBuilderValidationError queryObjectBuilderValidationError) {
                throw new WrongQueryExecutionException("Even with simplified parameters, execution of words list extraction operation is impossible.");
            }
        }

        final int finalPageId = pageId;
        final WordsSortFieldName finalWordsSortFieldName = wordsSortFieldName;
        final Order finalOrder = order;
        final String finalNgramData = ngramData;
        model.addAttribute("sortInfo", new HashMap<String, String>() {{
            put("URISortMoulderCurrentCollectionPath", COLLECTION_URL_SERVLET_IS_MAPPED + "/" + finalNgramData);
            put("URISortMoulderCurrentPageNumber", String.valueOf(finalPageId));
            put("URISortMoulderFieldToSort", finalWordsSortFieldName != null ? finalWordsSortFieldName.name() : null);
            put("URISortMoulderOrder", finalOrder != null ? finalOrder.name() : null);
            if (isSearchResult) {
                put("URISortMoulderAdditionalParameters", "?query=" + patternToFilter);
            }
        }});

        model.addAttribute("paginatorAmountOfPages", amountOfPages);
        model.addAttribute("paginatorCurrentPage", finalPageId);
        model.addAttribute("paginatorRestCollectionPath", COLLECTION_URL_SERVLET_IS_MAPPED + "/" + ngramData);
        if (isSearchResult) {
            model.addAttribute("paginatorRestQueryParameters", pathPostConditions + "?query=" + patternToFilter);
        } else {
            model.addAttribute("paginatorRestQueryParameters", pathPostConditions);
        }

        model.addAttribute("searchInfo", new HashMap<String, String>() {{
            put("searchPlaceholder", SEARCH_PLACEHOLDER);
            if (!patternToFilter.equals(".")) {
                put("searchValue", patternToFilter);
            }
            put("searchAction", "/ngram/" + finalNgramData + "/");
        }});

        final List<WordForNgramWithCountDto> finalListOfWordsForNgram = listOfWordsForNgram;
        model.addAttribute("listResult", new HashMap<String, List>() {{
            put("listOfWordsForNgram", finalListOfWordsForNgram);
        }});
        model.addAttribute("ngramDataString", ngramData);
    }

    @RequestMapping(value =
            {
                    "/{ngram:\\w+}",
                    "/{ngram:\\w+}/{pageId:\\d+}/",
                    "/{ngram:\\w+}/{pageId:\\d+}/{wordsSortFieldName:word|count}/",
                    "/{ngram:\\w+}/{pageId:\\d+}/{wordsSortFieldName:word|count}/{order:asc|desc}/"
            }, method = RequestMethod.GET)
    public String printWordsToNgramPage(Model model, HttpServletRequest request, @RequestParam(value = "query", required = false) String searchQuery) throws WrongQueryExecutionException {
        logger.debug("Displaying page with the list of ngrams.");

        if (StringUtils.isEmpty(searchQuery)) {
            logger.debug("Showing an an ngram page (words for ngram and their amount).");
            GetItemsTable(model, request, REGEX_PATTERN_TO_EXTRACT_ALL_WORDS, false);

            return "ngram/view";
        } else {
            logger.debug("Showing a search result page.");
            GetItemsTable(model, request, searchQuery, true);
            model.addAttribute("searchWordPatternDataString", searchQuery);

            return "ngram/search-result";
        }
    }
}
