package controllers;

import databaseServicesApi.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/ngrams")
public class NgramsListAndNgramInformation {
    private static final String COLLECTION_URL_SERVLET_IS_MAPPED = "/ngrams/view";
    private static final int AMOUNT_OF_ELEMENTS_PER_PAGE = 10;
    private static final Logger logger = LoggerFactory.getLogger(NgramsListAndNgramInformation.class);

    private static final String SEARCH_PLACEHOLDER = "Search for ngrams (full match)";

    @Value("#{ngramsJdbcReader}")
    NgramsDatabaseReaderInterface dbreader;

    @RequestMapping(value =
            {
                    "/view",
                    "/view/{pageId:\\d+}/",
                    "/view/{pageId:\\d+}/{ngramsSortFieldName:name|count}/",
                    "/view/{pageId:\\d+}/{ngramsSortFieldName:name|count}/{order:asc|desc}/"
            }, method = RequestMethod.GET)
    public String printNgramsListPage(Model model, HttpServletRequest request, @RequestParam(value = "query", required = false) String searchQuery) throws WrongQueryExecutionException {
        logger.debug("Displaying page with the list of ngrams.");

        String pathConditionsPart = "";

        int pageId = 1;
        NgramsSortFieldName ngramsSortFieldName = null;
        Order order = null;
        Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if (pathVariables != null) {
            if (pathVariables.containsKey("pageId")) {
                pageId = NumberUtils.toInt("" + pathVariables.get("pageId"), pageId);
            }
            if (pathVariables.containsKey("ngramsSortFieldName")) {
                ngramsSortFieldName = NgramsSortFieldName.valueOf(pathVariables.get("ngramsSortFieldName").toString());
            }
            if (pathVariables.containsKey("order")) {
                order = Order.valueOf(pathVariables.get("order").toString());
            }
        }
        if (StringUtils.isEmpty(searchQuery)) {
            List<NgramDto> listOfNgrams;

            int amountOfPages = (int) Math.ceil((double) dbreader.getAmountOfNgramsInDatabase() / AMOUNT_OF_ELEMENTS_PER_PAGE);
            // In the case if input page is wrong or even negative, first page id will be applied by Rest Parser
            if (pageId > amountOfPages) {
                pageId = amountOfPages;
            } else {
                if (pageId <= 0) {
                    pageId = 1;
                }
            }

            try {
                listOfNgrams = dbreader.getListOfNgrams(
                        new NgramsListQueryObject.NgramsListQueryBuilder(AMOUNT_OF_ELEMENTS_PER_PAGE, pageId - 1)
                                .orderByField(ngramsSortFieldName)
                                .order(order).build()
                );
            } catch (QueryObjectBuilderValidationError e) {
                try {
                    listOfNgrams = dbreader.getListOfNgrams(
                            new NgramsListQueryObject.NgramsListQueryBuilder(AMOUNT_OF_ELEMENTS_PER_PAGE, 0).build()
                    );
                } catch (QueryObjectBuilderValidationError queryObjectBuilderValidationError) {
                    throw new WrongQueryExecutionException("Even with simplified parameters, execution of ngrams list extraction operation is impossible.");
                }
            }

            final int finalPageId = pageId;
            final NgramsSortFieldName finalNgramsSortFieldName = ngramsSortFieldName;
            final Order finalOrder = order;
            model.addAttribute("sortInfo", new HashMap<String, String>() {{
                put("URISortMoulderCurrentCollectionPath", COLLECTION_URL_SERVLET_IS_MAPPED);
                put("URISortMoulderCurrentPageNumber", String.valueOf(finalPageId));
                put("URISortMoulderFieldToSort", finalNgramsSortFieldName != null ? finalNgramsSortFieldName.name() : null);
                put("URISortMoulderOrder", finalOrder != null ? finalOrder.name() : null);
            }});

            model.addAttribute("paginatorAmountOfPages", amountOfPages);
            model.addAttribute("paginatorCurrentPage", pageId);
            model.addAttribute("paginatorRestCollectionPath", COLLECTION_URL_SERVLET_IS_MAPPED);
            model.addAttribute("paginatorRestQueryParameters", pathConditionsPart);

            final List<NgramDto> finalListOfNgrams = listOfNgrams;
            model.addAttribute("listResult", new HashMap<String, List>() {{
                put("listOfNgrams", finalListOfNgrams);
            }});

            model.addAttribute("searchInfo", new HashMap<String, String>() {{
                put("searchPlaceholder", SEARCH_PLACEHOLDER);
            }});

            return "ngrams/view";
        } else {
            logger.debug("User input is search query " + searchQuery);
            final NgramDto ngram = dbreader.searchNgramByDataFullMatch(searchQuery);

            model.addAttribute("searchResult", new HashMap<String, NgramDto>() {{
                put("ngram", ngram);
            }});

            Map<String, String> searchInfo = new HashMap<String, String>() {{
                put("searchPlaceholder", SEARCH_PLACEHOLDER);
            }};

            if (StringUtils.isNotEmpty(searchQuery)) {
                searchInfo.put("searchValue", searchQuery);
            }
            model.addAttribute("searchInfo", searchInfo);

            return "ngrams/search-result";
        }
    }
}
