<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="../includes/common-header.jspf" %>
    <title>500 – Internal Server Error</title>
    <style>
        img {
            display: block;
            float: right;
            position: absolute;
            margin-top: -7%;
            margin-left: 72%;
            width: 400px;
            height: auto;

            overflow: scroll;

            image-rendering: optimizeSpeed;
            image-rendering: -moz-crisp-edges;          /* Firefox */
            image-rendering: -o-crisp-edges;            /* Opera */
            image-rendering: -webkit-optimize-contrast; /* Chrome (and eventually Safari) */
            image-rendering: optimize-contrast;         /* CSS3 Proposed */
            -ms-interpolation-mode: nearest-neighbor;   /* IE8+ */
        }
    </style>
</head>
<body style="background-color: #2067B2; width: 100%; height: 100%; overflow: hidden;">

<%@ include file="../includes/common-navbar.jspf" %>

<div class="container" id="container">
    <!-- This page has a lot of css code and is harmful for brain -->
    <h1 style="width: 500px; color: #FFFFFF;">500 – Internal Server Error</h1>
    <img src="${pageContext.request.contextPath}/static/img/meditation_32px.png">
    <br/>
    <br/>
    <br/>

    <h2 style="width: 500px; color: #FFFFFF;">Something went wrong.</h2>
    <br/>
    <br/>

    <buttons:back-to-main-page-button/>
</div>

<%@ include file="../includes/common-footer.jspf" %>
</body>
</html>