<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page trimDirectiveWhitespaces="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="includes/common-header.jspf" %>
    <title>About this viewer</title>
</head>
<body>

<%@ include file="includes/common-navbar.jspf" %>

<div class="container" id="container">
    <div class="span6 center-element">

        <div class="text-center">
            <h1>N-grams statistic web viewer Happy New Year Edition</h1>
            <p>By Vadim Ne. as a task of internship project for Grid Dynamics.
                4th task was separated into another project.</p>
            <p>Kharkiv, autumn-winter 2013</p>
            <br />
            <br />
            <br />
            <buttons:back-to-main-page-button/>
        </div>
    </div>
</div>

<%@ include file="includes/common-footer.jspf" %>
</body>
</html>