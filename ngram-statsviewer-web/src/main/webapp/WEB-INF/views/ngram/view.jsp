<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>

<jsp:useBean id="sortInfo" scope="request" type="java.util.Map"/>
<jsp:useBean id="ngramDataString" scope="request" type="java.lang.String"/>
<%-- have listOfWordsForNgram inside: --%>
<jsp:useBean id="listResult" scope="request" type="java.util.Map<java.lang.String,java.util.List>"/>
<%--<jsp:useBean id="listOfWordsForNgram" scope="request" type="java.util.List<dao.entities.logical.WordForNgramWithCountDto>"/>--%>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="../includes/common-header.jspf" %>
    <title>View words for n-gram '${ngramDataString}'</title>
</head>
<body>

<%@ include file="../includes/common-navbar.jspf" %>

<div class="container" id="container">
    <div class="span6 center-element">
        <h1>N-gram: '${ngramDataString}'</h1>
        <br/>
        <%@ include file="../includes/search.jspf" %>
        <br/>

        <c:choose>
            <c:when test="${listResult.listOfWordsForNgram != null}">
                <table id="content-table" class="table table-bordered table-striped data-table sort display">
                    <thead>
                    <tr>
                        <th class="sorting">Words <buttons:table-sort-icon-generator sortInfo="${sortInfo}"
                                                                                     currentFieldName="word"/></th>
                        <th class="sorting">N-Grams count <buttons:table-sort-icon-generator sortInfo="${sortInfo}"
                                                                                             currentFieldName="count"/></th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:forEach items="${listResult.listOfWordsForNgram}" var="word">
                        <tr>
                            <td><c:out value="${word.word}"/></td>
                            <td><c:out value="${word.count}"/></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br/>
                <%@ include file="../includes/pagination.jspf" %>

            </c:when>
            <c:otherwise>
                <h1>Nothing found</h1>

                <p>Seems, that we have no ngram '${ngramDataString}' in our database. Try to find another one.</p>
                <br/>
                <br/>
                <buttons:back-to-main-page-button/>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<%@ include file="../includes/common-footer.jspf" %>
</body>
</html>