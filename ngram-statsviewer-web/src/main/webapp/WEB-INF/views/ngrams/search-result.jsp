<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%--
    The search result component. To use this component you need to include it into page and pass through the model:
    -- searchResult Map

    This map contains <String, NgramDto> values and it may contain "ngram" key.
    This key can contain object of NgramDto type, if backend found appropriate n-ggram by its data field
    or null, if nothing found.
--%>

<jsp:useBean id="searchResult" type="java.util.Map" scope="request"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="../includes/common-header.jspf" %>
    <title>Search results for ${param.query}</title>
</head>
<body>

<%@ include file="../includes/common-navbar.jspf" %>

<div class="container" id="container">
    <div class="span6 center-element">
        <h1>Search results for n-gram '${param.query}'</h1>
        <br/>
        <%@ include file="../includes/search.jspf" %>
        <br/>

        <div class="row">
            <c:choose>
                <c:when test="${searchResult.ngram == null}">
                    <h2 class="text-center">Nothing found. Try another query.</h2>
                </c:when>
                <c:otherwise>
                    <h2>N-gram: '<a href="<c:url value="/ngram/${searchResult.ngram.data}"/>">${searchResult.ngram.data}</a>'</h2>
                    <br/>
                    <p>Repeats <strong>${searchResult.ngram.count}</strong> times in dataset.</p>
                </c:otherwise>
            </c:choose>
            <br/>
            <br/>
            <p class="text-center">
                <a href="<c:url value="${pageContext.request.contextPath}/ngrams/view"/>" class="btn btn-primary btn-large">
                    <i class="icon-white icon-align-justify"></i> Go back to n-grams list</a>
            </p>
        </div>
    </div>
</div>

<%@ include file="../includes/common-footer.jspf" %>
</body>
</html>