<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>

<jsp:useBean id="sortInfo" scope="request" type="java.util.Map"/>
<%-- have listOfNgrams inside: --%>
<jsp:useBean id="listResult" scope="request" type="java.util.Map<java.lang.String,java.util.List>"/>
<%--<jsp:useBean id="listOfNgrams" scope="request" type="java.util.List<dao.entities.dbTables.NgramDto>"/>--%>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="../includes/common-header.jspf" %>
    <title>View list of n-grams</title>
</head>
<body>

<%@ include file="../includes/common-navbar.jspf" %>

<div class="container" id="container">
    <div class="span6 center-element">
        <h1>List of all n-grams</h1>
        <br/>
        <%@ include file="../includes/search.jspf" %>
        <br/>

        <c:choose>
            <c:when test="${listResult.listOfNgrams != null}">
                <table id="content-table" class="table table-bordered table-striped data-table sort display">
                    <thead>
                    <tr>
                        <th class="sorting">N-Gram <buttons:table-sort-icon-generator sortInfo="${sortInfo}"
                                                                                      currentFieldName="name"/></th>
                        <th class="sorting">Count <buttons:table-sort-icon-generator sortInfo="${sortInfo}"
                                                                                     currentFieldName="count"/></th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:forEach items="${listResult.listOfNgrams}" var="ngramItem">
                        <tr>
                            <td><a href="<c:url value="/ngram/${ngramItem.data}"/>">
                                <c:out value="${ngramItem.data}"/></a>
                            </td>
                            <td><c:out value="${ngramItem.count}"/></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br/>
                <%@ include file="../includes/pagination.jspf" %>

            </c:when>
            <c:otherwise>
                <h1>Nothing found</h1>

                <p>Seems, that this list is empty. Fill database, please.</p>
                <br/>
                <br/>
                <buttons:back-to-main-page-button/>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<%@ include file="../includes/common-footer.jspf" %>
</body>
</html>