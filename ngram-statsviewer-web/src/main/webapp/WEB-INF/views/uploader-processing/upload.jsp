<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page trimDirectiveWhitespaces="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="../includes/common-header.jspf" %>
    <title>Upload file for processing</title>
</head>
<body>

<%@ include file="../includes/common-navbar.jspf" %>

<div class="container" id="container">
    <div class="span6 center-element">
        <form name="fileProcessingForm" id="fileProcessingForm" class="validErrors" enctype="multipart/form-data"
              method="POST" action="${pageContext.request.contextPath}/upload">

            <label>Ngram Length</label>
            <div class="input-prepend"><span class="add-on"><i class="icon-filter"></i></span>
                <input type="text" name="ngramLength" placeholder="Ngram Length">
            </div>

            <br />

            <label>File To Process (*.txt only, up to 2MB)</label>
            <input type="file" name="fileToProcess" />

            <br />
            <br />

            <button type="submit" class="btn btn-large btn-success">Submit</button>
        </form>
    </div>
</div>

<%@ include file="../includes/common-footer.jspf" %>
</body>
</html>