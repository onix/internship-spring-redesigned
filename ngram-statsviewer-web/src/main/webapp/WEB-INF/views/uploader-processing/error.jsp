<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page trimDirectiveWhitespaces="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="../includes/common-header.jspf" %>
    <title>File processing error</title>
</head>
<body>

<%@ include file="../includes/common-navbar.jspf" %>

<div class="container" id="container">
    <div class="span6 center-element">
        <h1>An error while file processing occured!</h1>
        <p>Please, ${errorFileProcessingMessage}</p>
        <br/>
        <br/>
        <buttons:back-to-main-page-button/>
    </div>
</div>

<%@ include file="../includes/common-footer.jspf" %>
</body>
</html>