<%@ tag trimDirectiveWhitespaces="true" %>

<%@ attribute name="current" required="true" type="java.lang.Integer" %>
<%@ attribute name="total" required="true" type="java.lang.Integer" %>

<li class="active"><a href="#"> ${current} / ${total} </a></li>