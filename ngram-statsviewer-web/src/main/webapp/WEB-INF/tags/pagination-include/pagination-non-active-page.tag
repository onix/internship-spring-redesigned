<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag trimDirectiveWhitespaces="true" %>

<%@ attribute name="paginatorPathPrePart" required="true" type="java.lang.String" %>
<%@ attribute name="paginatorPathPostPart" required="true" type="java.lang.String" %>
<%@ attribute name="p" required="true" type="java.lang.Integer" %>

<li><a href="<c:url value="${paginatorPathPrePart}/${p}/${paginatorPathPostPart}"/>"> ${p} </a></li>