<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag trimDirectiveWhitespaces="true" %>

<%@ attribute name="value" required="true" type="java.lang.String" %>

<c:out value="${pageContext.request.contextPath}/static/${value}"/>