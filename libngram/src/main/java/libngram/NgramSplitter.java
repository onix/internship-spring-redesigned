package libngram;

import java.util.ArrayList;
import java.util.List;

public class NgramSplitter {

    public static WordWithNgrams splitWordOnNgrams(int wordInPhraseId, String word, int ngramLength) {
        if (word == null || ngramLength < 1)
            return null;

        int beginIndex = 0;
        int endIndex = ngramLength;

        List<String> ngramsList = new ArrayList<String>();

        while (endIndex <= word.length()) {
            ngramsList.add(word.substring(beginIndex, endIndex));

            beginIndex++;
            endIndex = beginIndex + ngramLength;
        }

        return new WordWithNgrams(wordInPhraseId, word, ngramsList);
    }
}
