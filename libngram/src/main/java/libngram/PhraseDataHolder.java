package libngram;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PhraseDataHolder {
    private final String phrase;
    private final List<WordWithNgrams> wordsWithNgramsList;

    public PhraseDataHolder(String phrase, List<WordWithNgrams> wordsWithNgramsList) {
        if (phrase == null || wordsWithNgramsList == null) {
            throw new IllegalArgumentException("Object has got null as its input parameters.");
        }
        this.phrase = phrase;
        this.wordsWithNgramsList = Collections.unmodifiableList(new ArrayList<WordWithNgrams>(wordsWithNgramsList));
    }

    public String getPhrase() {
        return phrase;
    }

    public List<WordWithNgrams> getWordsWithNgramsList() {
        return wordsWithNgramsList;
    }

    // These methods implemented for testing purposes at the moment
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhraseDataHolder that = (PhraseDataHolder) o;

        return phrase.equals(that.phrase) && wordsWithNgramsList.equals(that.wordsWithNgramsList);

    }

    @Override
    public int hashCode() {
        int result = phrase.hashCode();
        result = 31 * result + wordsWithNgramsList.hashCode();
        return result;
    }
}
