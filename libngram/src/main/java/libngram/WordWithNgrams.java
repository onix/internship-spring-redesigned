package libngram;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WordWithNgrams {
    private final int wordInPhraseId;
    private final String word;
    private final List<String> ngramsListData;

    public WordWithNgrams(int wordInPhraseId, String word, List<String> ngramsListData) {
        if (word == null || wordInPhraseId < 0) {
            throw new IllegalArgumentException("Object has got null as its input parameters.");
        }

        this.wordInPhraseId = wordInPhraseId;
        this.word = word;
        if (ngramsListData != null) {
            this.ngramsListData = Collections.unmodifiableList(new ArrayList<String>(ngramsListData));
        } else {
            this.ngramsListData = null;
        }
    }

    public String getWord() {
        return word;
    }

    public List<String> getNgramsList() {
        return ngramsListData;
    }

    // These methods implemented for testing purposes at the moment
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WordWithNgrams that = (WordWithNgrams) o;

        return wordInPhraseId == that.wordInPhraseId && !(ngramsListData != null ? !ngramsListData.equals(that.ngramsListData) : that.ngramsListData != null) && word.equals(that.word);

    }

    @Override
    public int hashCode() {
        int result = wordInPhraseId;
        result = 31 * result + word.hashCode();
        result = 31 * result + (ngramsListData != null ? ngramsListData.hashCode() : 0);
        return result;
    }
}