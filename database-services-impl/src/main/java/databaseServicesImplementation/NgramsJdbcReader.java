package databaseServicesImplementation;

import dao.NgramsDao;
import dao.daoQueryObjects.NgramsListDaoQueryObject;
import dao.entities.Ngram;
import databaseServicesApi.NgramDto;
import databaseServicesApi.NgramsDatabaseReaderInterface;
import databaseServicesApi.NgramsListQueryObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class NgramsJdbcReader implements NgramsDatabaseReaderInterface {
    @Value("#{jdbcNgramsDao}")
    private NgramsDao ngramsDao;

    @Override
    public NgramDto searchNgramByDataFullMatch(String ngramData) {
        Ngram ngram = ngramsDao.findByDataString(ngramData);
        if (ngram != null) {
            return new NgramDto(ngram.getData(), ngram.getCount());
        } else return null;
    }

    @Override
    public List<NgramDto> getListOfNgrams(NgramsListQueryObject ngramsListQuery) {
        if (ngramsListQuery == null)
            return null;

        NgramsListDaoQueryObject ngramsDaoQO = NgramsListQueryObjectToDaoQueryObjectConverter.convert(ngramsListQuery);
        return NgramsListDaoToDto.convert(ngramsDao.extractListOfNgrams(ngramsDaoQO));
    }

    @Override
    public int getAmountOfNgramsInDatabase() {
        return ngramsDao.extractAmountOfNgrams();
    }
}
