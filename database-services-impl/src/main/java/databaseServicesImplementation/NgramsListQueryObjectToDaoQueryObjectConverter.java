package databaseServicesImplementation;

import dao.daoQueryObjects.NgramsListDaoQueryObject;
import databaseServicesApi.NgramsListQueryObject;

public class NgramsListQueryObjectToDaoQueryObjectConverter {
    public static NgramsListDaoQueryObject convert(NgramsListQueryObject ngramsListQuery) {
        return new NgramsListDaoQueryObject(
                ngramsListQuery.getAmount(),
                ngramsListQuery.getOffsetPage(),
                ngramsListQuery.getOrderByFieldName(),
                ngramsListQuery.getOrder());
    }
}
