package databaseServicesImplementation;

import dao.NgramsDao;
import dao.WordsDao;
import dao.daoQueryObjects.WordsListDaoQueryObject;
import databaseServicesApi.WordForNgramWithCountDto;
import databaseServicesApi.WordsDatabaseReaderInterface;
import databaseServicesApi.WordsListQueryObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(value = "wordsDatabaseReader")
@Transactional(readOnly = true)
public class WordsJdbcReader implements WordsDatabaseReaderInterface {
    @Value("#{jdbcWordsDao}")
    private WordsDao wordsDao;
    @Value("#{jdbcNgramsDao}")
    private NgramsDao ngramsDao;

    @Override
    public List<WordForNgramWithCountDto> getListOfWordsWithCounters(WordsListQueryObject wordsListQuery) {
        if (wordsListQuery == null)
            return null;

        int ngramId = ngramsDao.getIdForNgram(wordsListQuery.getNgramData());
        WordsListDaoQueryObject wrdsDaoQO = WordsListQueryObjectToDaoQueryObjectConverter.convert(ngramId, wordsListQuery);
        return WordsListDaoToDto.convert(wordsDao.findAndExtractListOfWordsForNgramByPartialWordMatch(wrdsDaoQO));
    }

    @Override
    public int getAmountOfWordsWithCountersInDatabase(String ngramData, String partOfWord) {
        if (StringUtils.isEmpty(ngramData) || StringUtils.isEmpty(partOfWord))
            return 0;

        int ngramId = ngramsDao.getIdForNgram(ngramData);
        if (ngramId > 0) {
            return wordsDao.extractAmountOfWordsForNgramByItsDataPartialMatch(ngramId, partOfWord);
        }
        return 0;
    }
}
