package databaseServicesImplementation;

import dao.daoQueryObjects.WordsListDaoQueryObject;
import databaseServicesApi.WordsListQueryObject;

public class WordsListQueryObjectToDaoQueryObjectConverter {
    public static WordsListDaoQueryObject convert(int ngramId, WordsListQueryObject wordsListQuery) {
        return new WordsListDaoQueryObject(ngramId,
                wordsListQuery.getOffsetPage(),
                wordsListQuery.getAmountOfElements(),
                wordsListQuery.getWordToSearchPattern(),
                wordsListQuery.getOrderByFieldName(),
                wordsListQuery.getOrder());
    }
}
