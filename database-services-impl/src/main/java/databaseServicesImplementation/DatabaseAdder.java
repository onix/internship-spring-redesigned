package databaseServicesImplementation;

import dao.*;
import dao.entities.Ngram;
import dao.entities.NgramToWord;
import dao.entities.WordToPhrase;
import dao.jdbcDao.IllegalInputSizeException;
import databaseServicesApi.DatabaseAdderInterface;
import libngram.PhraseDataHolder;
import libngram.WordWithNgrams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class DatabaseAdder implements DatabaseAdderInterface {
    @Autowired
    private PhrasesDao phrasesDao;
    @Autowired
    private WordsDao wordsDao;
    @Autowired
    private NgramsDao ngramsDao;
    @Autowired
    private WordsToPhrasesDao wordsToPhrasesDao;
    @Autowired
    private NgramsToWordsDao ngramsToWordsDao;

    /**
     * Remove all data from all tables.
     */
    @Override
    public void clearDatabase() {
        ngramsToWordsDao.removeAllEntetiesFromTable();
        wordsToPhrasesDao.removeAllEntetiesFromTable();
        ngramsDao.removeAllEntetiesFromTable();
        wordsDao.removeAllEntetiesFromTable();
        phrasesDao.removeAllEntetiesFromTable();
    }

    @Override
    public void addToDatabase(PhraseDataHolder data) {
        try {
            // Insert phrase into table
            int phraseDbId = phrasesDao.insertNewPhrase(data.getPhrase());

            // Insert Elements into table
            for (WordWithNgrams wordWithNgrams : data.getWordsWithNgramsList()) {
                String currentWordData = wordWithNgrams.getWord();

                boolean isWordNew = false;

                int wordDbId = wordsDao.findWordId(currentWordData);
                if (wordDbId == 0) {
                    wordDbId = wordsDao.insertNewWord(currentWordData);
                    isWordNew = true;
                }

                WordToPhrase wordToPhrase = wordsToPhrasesDao.findWordToPhrase(wordDbId, phraseDbId);
                if (wordToPhrase == null) {
                    wordsToPhrasesDao.insertNewWordToPhrase(wordDbId, phraseDbId);
                } else {
                    wordToPhrase.incrementCounter();
                    wordsToPhrasesDao.updateWordToPhraseCounter(wordToPhrase);
                }

                if (wordWithNgrams.getNgramsList() != null) {
                    for (String currentNgramData : wordWithNgrams.getNgramsList()) {
                        // Find and insert ngram into dbTables or increment ngram counter
                        Ngram ngram = ngramsDao.findByDataString(currentNgramData);
                        if (ngram == null) {
                            int newNgramDbIndex = ngramsDao.insertNewNgram(currentNgramData);
                            ngram = new Ngram(newNgramDbIndex, currentNgramData, 1);
                        } else {
                            ngram.incrementCounter();
                            ngramsDao.updateNgramCounter(ngram);
                        }

                        // Find and insert ngram_to_word relation of increment ngram in word counter
                        NgramToWord ngramToWord = ngramsToWordsDao.findNgramToWord(ngram.getId(), wordDbId);
                        if (ngramToWord == null) {
                            ngramsToWordsDao.insertNewNgramsToWords(ngram.getId(), wordDbId);
                        } else {
                            if (isWordNew) {
                                ngramToWord.incrementCounter();
                                ngramsToWordsDao.updateNgramToWordCounter(ngramToWord);
                            }
                        }
                    }
                }
            }
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }
}
