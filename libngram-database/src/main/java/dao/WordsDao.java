package dao;

import dao.daoQueryObjects.WordsListDaoQueryObject;
import dao.entities.WordForNgramWithCount;

import java.util.List;

public interface WordsDao {

    int findWordId(String wordDataString);

    List<WordForNgramWithCount> findAndExtractListOfWordsForNgramByPartialWordMatch(WordsListDaoQueryObject wordsListDaoQueryObject);

    int extractAmountOfWordsForNgramByItsDataPartialMatch(int ngramData, String partOfWord);

    int insertNewWord(String wordString);

    void removeAllEntetiesFromTable();
}
