package dao.jdbcDao;

import dao.AbstractDao;
import dao.WordsDao;
import dao.daoQueryObjects.Order;
import dao.daoQueryObjects.WordsListDaoQueryObject;
import dao.daoQueryObjects.WordsSortFieldName;
import dao.entities.WordForNgramWithCount;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcWordsDao extends AbstractDao implements WordsDao {
    private final static String SELECT_WORD_ID_BY_DATA = "SELECT ID FROM WORDS WHERE word_data = ?";
    private final static String INSERT_WORD = "INSERT INTO WORDS(WORD_DATA) VALUES (?)";
    private final static String CLEAN_WORDS_TABLE = "DELETE FROM WORDS";

    private final static String COUNT_TEN_WORDS_FOR_NGRAM_PARTIAL_MATCH =
            "SELECT COUNT(*) FROM WORDS INNER JOIN N_GRAMS_TO_WORDS ON ID = WORD_ID WHERE NGRAM_ID = ? AND WORD_DATA REGEXP ?";
    //------------------------------------------------------------------------------------------------------------------
    private final static String SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART =
            "SELECT WORD_DATA, NGRAM_IN_WORD_COUNT FROM WORDS AS A INNER JOIN N_GRAMS_TO_WORDS AS B ON A.ID = B.WORD_ID WHERE NGRAM_ID = ? AND WORD_DATA REGEXP ? ";

    private final static String SELECT_TEN_WORDS_QUERY_DATA_FIELD_NAME_PART = " WORD_DATA ";
    private final static String SELECT_TEN_WORDS_QUERY_COUNTER_FIELD_PART = " NGRAM_IN_WORD_COUNT ";
    //******************************************************************************************************************
    private final static String QUERY_ORDER_BY_CLAUSE = " ORDER BY ";
    private final static String QUERY_LIMITATION_PART = " LIMIT ? OFFSET ? * 10;";
    //******************************************************************************************************************

    @Override
    public int findWordId(String wordDataString) {
        if (wordDataString == null) {
            return 0;
        }
        try {
            return getJdbcTemplate().queryForInt(SELECT_WORD_ID_BY_DATA, wordDataString);
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public List<WordForNgramWithCount> findAndExtractListOfWordsForNgramByPartialWordMatch(WordsListDaoQueryObject wordsListDaoQueryObject) {
        if (wordsListDaoQueryObject == null) {
            return null;
        }
        String querySB;

        int ngramId = wordsListDaoQueryObject.getNgramId();
        String partOfWord = wordsListDaoQueryObject.getPartOfWord();
        int amountOfItems = wordsListDaoQueryObject.getAmountOfItems();
        int offsetPage = wordsListDaoQueryObject.getOffsetPage();

        if (wordsListDaoQueryObject.getOrder() == null && wordsListDaoQueryObject.getOrderByFieldName() == null) {
            querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART + QUERY_LIMITATION_PART;
        } else {
            if (wordsListDaoQueryObject.getOrder() == Order.asc) {
                if (wordsListDaoQueryObject.getOrderByFieldName() == WordsSortFieldName.word) { // ASC by word
                    querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART +
                            QUERY_ORDER_BY_CLAUSE + SELECT_TEN_WORDS_QUERY_DATA_FIELD_NAME_PART + "ASC" + QUERY_LIMITATION_PART;
                } else { // ASC by count
                    querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART +
                            QUERY_ORDER_BY_CLAUSE + SELECT_TEN_WORDS_QUERY_COUNTER_FIELD_PART + "ASC" + QUERY_LIMITATION_PART;
                }
            } else {
                if (wordsListDaoQueryObject.getOrderByFieldName() == WordsSortFieldName.word) { // DESC by word
                    querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART +
                            QUERY_ORDER_BY_CLAUSE + SELECT_TEN_WORDS_QUERY_DATA_FIELD_NAME_PART + "DESC" + QUERY_LIMITATION_PART;
                } else { // DESC by count
                    querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART +
                            QUERY_ORDER_BY_CLAUSE + SELECT_TEN_WORDS_QUERY_COUNTER_FIELD_PART + "DESC" + QUERY_LIMITATION_PART;
                }
            }
        }

        List<WordForNgramWithCount> listOfWordsForNgram = new ArrayList<WordForNgramWithCount>();
        List<Map<String, Object>> rows = getJdbcTemplate().queryForList(querySB, ngramId, partOfWord, amountOfItems, offsetPage);

        if ((rows != null) && (rows.size() > 0)) {
            for (Map<String, Object> row : rows) {
                listOfWordsForNgram.add(new WordForNgramWithCount(
                        (String) row.get("WORD_DATA"),
                        (Integer) row.get("NGRAM_IN_WORD_COUNT")
                ));
            }
        } else {
            return null;
        }
        return listOfWordsForNgram;
    }

    @Override
    public int extractAmountOfWordsForNgramByItsDataPartialMatch(int ngramData, String partOfWord) {
        if (ngramData > 0 || partOfWord != null) {
            return getJdbcTemplate().queryForInt(COUNT_TEN_WORDS_FOR_NGRAM_PARTIAL_MATCH, ngramData, partOfWord);
        }
        return 0;
    }

    @Override
    public int insertNewWord(final String wordString) {
        if (wordString == null) {
            return 0;
        }

        KeyHolder keyHolder = new GeneratedKeyHolder();
        getJdbcTemplate().update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement ps = con.prepareStatement(INSERT_WORD, new String[]{"id"});
                        ps.setString(1, wordString);
                        return ps;
                    }
                }, keyHolder
        );
        return keyHolder.getKey().intValue();

    }

    @Override
    public void removeAllEntetiesFromTable() {
        getJdbcTemplate().update(CLEAN_WORDS_TABLE);
    }
}
