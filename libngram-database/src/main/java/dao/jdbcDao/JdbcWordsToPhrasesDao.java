package dao.jdbcDao;

import dao.AbstractDao;
import dao.WordsToPhrasesDao;
import dao.entities.WordToPhrase;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcWordsToPhrasesDao extends AbstractDao implements WordsToPhrasesDao {
    private final static String SELECT_WORD_TO_PHRASE = "SELECT * FROM WORDS_TO_PHRASES WHERE WORD_ID = ? AND PHRASE_ID = ?";
    private final static String INSERT_NEW_WORD_TO_PHRASE = "INSERT INTO WORDS_TO_PHRASES VALUES (?, ?, 1);";
    private final static String UPDATE_WORDS_TO_PHRASES_COUNTER = "UPDATE WORDS_TO_PHRASES SET WORDS_IN_PHRASE_COUNT = ? WHERE WORD_ID = ? AND PHRASE_ID = ?";
    private final static String CLEAN_WORDS_TO_PHRASES_TABLE = "DELETE FROM WORDS_TO_PHRASES;";

    @Override
    public WordToPhrase findWordToPhrase(int wordId, int phraseId) {
        if (wordId == 0 || phraseId == 0) {
            return null;
        }

        try {
            return getJdbcTemplate().queryForObject(SELECT_WORD_TO_PHRASE, new Object[]{wordId, phraseId},
                    new RowMapper<WordToPhrase>() {
                        @Override
                        public WordToPhrase mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return new WordToPhrase(
                                    rs.getInt(WordToPhrase.WORD_TO_PHRASE_WORD_ID_COLUMN_NAME),
                                    rs.getInt(WordToPhrase.WORD_TO_PHRASE_PHRASE_ID_COLUMN_NAME),
                                    rs.getInt(WordToPhrase.WORD_TO_PHRASE_WORDS_IN_PHRASE_COUNT_COLUMN_NAME)
                            );
                        }
                    });
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public WordToPhrase insertNewWordToPhrase(final int wordId, final int phraseId) {
        if (wordId == 0 && phraseId == 0) {
            return null;
        }

        int numberOfRowsAffected = getJdbcTemplate().update(INSERT_NEW_WORD_TO_PHRASE, wordId, phraseId);
        if (numberOfRowsAffected > 0) {
            return new WordToPhrase(wordId, phraseId, 1);
        } else {
            return null;
        }
    }

    @Override
    public void updateWordToPhraseCounter(WordToPhrase wordToPhrase) {
        if (wordToPhrase != null) {
            getJdbcTemplate().update(UPDATE_WORDS_TO_PHRASES_COUNTER, wordToPhrase.getWordToPhraseCount(), wordToPhrase.getWordId(), wordToPhrase.getPhraseId());
        }
    }

    @Override
    public void removeAllEntetiesFromTable() {
        getJdbcTemplate().update(CLEAN_WORDS_TO_PHRASES_TABLE);
    }
}
