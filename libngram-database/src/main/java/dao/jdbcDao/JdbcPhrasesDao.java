package dao.jdbcDao;

import dao.AbstractDao;
import dao.PhrasesDao;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Repository
public class JdbcPhrasesDao extends AbstractDao implements PhrasesDao {
    private final static String INSERT_PHRASE = "INSERT INTO PHRASES(PHRASE_DATA) VALUES(?)";
    private final static String CLEAN_PHRASES_TABLE = "DELETE FROM PHRASES";

    @Override
    public int insertNewPhrase(final String phraseString) {
        if (phraseString == null) {
            return 0;
        }

        KeyHolder keyHolder = new GeneratedKeyHolder();
        getJdbcTemplate().update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement ps = con.prepareStatement(INSERT_PHRASE, new String[]{"id"});
                        ps.setString(1, phraseString);
                        return ps;
                    }
                }, keyHolder
        );
        return keyHolder.getKey().intValue();
    }

    @Override
    public void removeAllEntetiesFromTable() {
        getJdbcTemplate().update(CLEAN_PHRASES_TABLE);
    }
}
