package dao.jdbcDao;

import dao.AbstractDao;
import dao.NgramsDao;
import dao.daoQueryObjects.NgramsListDaoQueryObject;
import dao.daoQueryObjects.NgramsSortFieldName;
import dao.daoQueryObjects.Order;
import dao.entities.Ngram;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcNgramsDao extends AbstractDao implements NgramsDao {
    private final static String SELECT_NGRAM_BY_NGRAM_STRING_DATA = "SELECT * FROM N_GRAMS WHERE N_GRAM_DATA = ?";
    private final static String SELECT_AMOUNT_OF_ELEMENTS = "SELECT COUNT(*) FROM N_GRAMS";
    private final static String INSERT_NEW_NGRAM = "INSERT INTO N_GRAMS(N_GRAM_DATA, N_GRAM_COUNT) VALUES (?, 1)";
    private final static String UPDATE_NGRAM_COUNTER = "UPDATE N_GRAMS SET N_GRAM_COUNT = ? WHERE ID = ?";
    private final static String CLEAN_NGRAMS_TABLE = "DELETE FROM N_GRAMS";
    //------------------------------------------------------------------------------------------------------------------
    private final static String SELECT_TEN_NGRAMS_GENERAL_PART = "SELECT * FROM N_GRAMS ";

    private final static String SELECT_TEN_NGRAMS_QUERY_DATA_FIELD_NAME_PART = " n_gram_data ";
    private final static String SELECT_TEN_NGRAMS_QUERY_COUNTER_FIELD_NAME_PART = " n_gram_count ";

    private final static String QUERY_ORDER_BY_CLAUSE = " ORDER BY ";
    private final static String QUERY_LIMITATION_PART = " LIMIT ? OFFSET ? * 10;";
    //******************************************************************************************************************

    @Override
    public Ngram findByDataString(String ngramDataString) {
        if (ngramDataString == null) {
            return null;
        }

        try {
            return getJdbcTemplate().queryForObject(SELECT_NGRAM_BY_NGRAM_STRING_DATA, new Object[]{ngramDataString},
                    new RowMapper<Ngram>() {
                        @Override
                        public Ngram mapRow(ResultSet rs, int rowNum) throws SQLException {
                            try {
                                return new Ngram(
                                        rs.getInt(Ngram.NGRAM_ID_COLUMN_NAME),
                                        rs.getString(Ngram.NGRAM_DATA_COLUMN_NAME),
                                        rs.getInt(Ngram.NGRAM_COUNT_COLUMN_NAME)
                                );
                            } catch (IllegalInputSizeException e) {
                                throw new RuntimeException("Invalid n-gram object from database extracted.");
                            }
                        }
                    });
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    @Override
    public int getIdForNgram(String ngramDataString) {
        Ngram ngram = findByDataString(ngramDataString);
        if (ngram == null) {
            return 0;
        }
        return ngram.getId();
    }

    @Override
    public List<Ngram> extractListOfNgrams(NgramsListDaoQueryObject ngramsListDaoQueryObject) {
        if (ngramsListDaoQueryObject == null) {
            return null;
        }

        String querySB;

        int amountOfItems = ngramsListDaoQueryObject.getAmount();
        int offsetPage = ngramsListDaoQueryObject.getOffsetPage();

        if (ngramsListDaoQueryObject.getOrder() == null && ngramsListDaoQueryObject.getOrderByFieldName() == null) {
            querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_LIMITATION_PART;
        } else {
            if (ngramsListDaoQueryObject.getOrder() == Order.asc) {
                if (ngramsListDaoQueryObject.getOrderByFieldName() == NgramsSortFieldName.name) { // ASC by name
                    querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_ORDER_BY_CLAUSE + SELECT_TEN_NGRAMS_QUERY_DATA_FIELD_NAME_PART + "ASC" + QUERY_LIMITATION_PART;
                } else { // ASC by count
                    querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_ORDER_BY_CLAUSE + SELECT_TEN_NGRAMS_QUERY_COUNTER_FIELD_NAME_PART + "ASC" + QUERY_LIMITATION_PART;
                }
            } else {
                if (ngramsListDaoQueryObject.getOrderByFieldName() == NgramsSortFieldName.name) { // DESC by name
                    querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_ORDER_BY_CLAUSE + SELECT_TEN_NGRAMS_QUERY_DATA_FIELD_NAME_PART + "DESC" + QUERY_LIMITATION_PART;
                } else { // DESC by count
                    querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_ORDER_BY_CLAUSE + SELECT_TEN_NGRAMS_QUERY_COUNTER_FIELD_NAME_PART + "DESC" + QUERY_LIMITATION_PART;
                }
            }
        }

        List<Ngram> listOfNgrams = new ArrayList<Ngram>();
        List<Map<String, Object>> rows = getJdbcTemplate().queryForList(querySB, amountOfItems, offsetPage);

        if ((rows != null) && (rows.size() > 0)) {
            for (Map<String, Object> row : rows) {
                try {
                    listOfNgrams.add(new Ngram(
                            (Integer) row.get(Ngram.NGRAM_ID_COLUMN_NAME),
                            (String) row.get(Ngram.NGRAM_DATA_COLUMN_NAME),
                            (Integer) row.get(Ngram.NGRAM_COUNT_COLUMN_NAME)
                    ));
                } catch (IllegalInputSizeException e) {
                    throw new RuntimeException("Data is not valid.", e);
                }
            }
        } else {
            return null;
        }
        return listOfNgrams;
    }

    @Override
    public int extractAmountOfNgrams() {
        return getJdbcTemplate().queryForInt(SELECT_AMOUNT_OF_ELEMENTS);
    }

    @Override
    public int insertNewNgram(final String ngramString) {
        if (ngramString == null) {
            return 0;
        }

        KeyHolder keyHolder = new GeneratedKeyHolder();
        getJdbcTemplate().update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement ps = con.prepareStatement(INSERT_NEW_NGRAM, new String[]{"id"});
                        ps.setString(1, ngramString);
                        return ps;
                    }
                }, keyHolder
        );
        return keyHolder.getKey().intValue();
    }

    @Override
    public void updateNgramCounter(Ngram ngram) {
        if (ngram != null) {
            getJdbcTemplate().update(UPDATE_NGRAM_COUNTER, ngram.getCount(), ngram.getId());
        }
    }

    @Override
    public void removeAllEntetiesFromTable() {
        getJdbcTemplate().update(CLEAN_NGRAMS_TABLE);
    }
}
