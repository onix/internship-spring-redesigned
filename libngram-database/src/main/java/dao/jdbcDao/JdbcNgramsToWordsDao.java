package dao.jdbcDao;

import dao.AbstractDao;
import dao.NgramsToWordsDao;
import dao.entities.NgramToWord;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcNgramsToWordsDao extends AbstractDao implements NgramsToWordsDao {
    private final static String SELECT_NGRAM_TO_WORD_BY_ID = "SELECT * FROM N_GRAMS_TO_WORDS WHERE NGRAM_ID = ? AND WORD_ID = ?";
    private final static String INSERT_NEW_NGRAM_TO_WORD = "INSERT INTO N_GRAMS_TO_WORDS VALUES (?, ?, 1)";
    private final static String UPDATE_NGRAM_TO_WORD_COUNTER = "UPDATE N_GRAMS_TO_WORDS SET NGRAM_IN_WORD_COUNT = ? WHERE NGRAM_ID = ? AND WORD_ID = ?";
    private final static String CLEAN_NGRAMS_TO_WORDS_TABLE = "DELETE FROM N_GRAMS_TO_WORDS";

    @Override
    public NgramToWord findNgramToWord(int ngramId, int wordId) {
        if (ngramId == 0 || wordId == 0) {
            return null;
        }

        try {
            return getJdbcTemplate().queryForObject(SELECT_NGRAM_TO_WORD_BY_ID, new Object[]{ngramId, wordId},
                    new RowMapper<NgramToWord>() {
                        @Override
                        public NgramToWord mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return new NgramToWord(
                                    rs.getInt(NgramToWord.NGRAM_TO_WORD_NGRAM_ID_COLUMN_NAME),
                                    rs.getInt(NgramToWord.NGRAM_TO_WORD_WORD_ID_COLUMN_NAME),
                                    rs.getInt(NgramToWord.NGRAM_TO_WORD_NGRAM_IN_WORD_COUNT_COLUMN_NAME)
                            );
                        }
                    });
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public NgramToWord insertNewNgramsToWords(int ngramId, int wordId) {
        if (ngramId == 0 && wordId == 0) {
            return null;
        }

        int numberOfRowsAffected = getJdbcTemplate().update(INSERT_NEW_NGRAM_TO_WORD, ngramId, wordId);
        if (numberOfRowsAffected > 0) {
            return new NgramToWord(ngramId, wordId, 1);
        } else {
            return null;
        }
    }

    @Override
    public void updateNgramToWordCounter(NgramToWord ngramToWord) {
        if (ngramToWord != null) {
            getJdbcTemplate().update(UPDATE_NGRAM_TO_WORD_COUNTER, ngramToWord.getNgramInWordCount(), ngramToWord.getNgramId(), ngramToWord.getWordId());
        }
    }

    @Override
    public void removeAllEntetiesFromTable() {
        getJdbcTemplate().update(CLEAN_NGRAMS_TO_WORDS_TABLE);
    }
}
