package dao.daoQueryObjects;

public class WordsListDaoQueryObject {
    // required
    private final int ngramId;
    private final int offsetPage;
    private final int amountOfItems;
    private final String partOfWord;

    // optional
    private final WordsSortFieldName orderByFieldName;
    private final Order order;

    public WordsListDaoQueryObject(int ngramId, int pageOffset, int amountOfItems, String partOfWord, Enum orderByFieldName, Enum order) {
        this.ngramId = ngramId;
        this.offsetPage = pageOffset;
        this.amountOfItems = amountOfItems;
        this.partOfWord = partOfWord;

        if (orderByFieldName == null)
            this.orderByFieldName = null;
        else
            this.orderByFieldName = WordsSortFieldName.valueOf(orderByFieldName.name());

        if (order == null)
            this.order = null;
        else
            this.order = Order.valueOf(order.name());
    }

    public int getNgramId() {
        return ngramId;
    }

    public int getOffsetPage() {
        return offsetPage;
    }

    public int getAmountOfItems() {
        return amountOfItems;
    }

    public String getPartOfWord() {
        return partOfWord;
    }

    public WordsSortFieldName getOrderByFieldName() {
        return orderByFieldName;
    }

    public Order getOrder() {
        return order;
    }
}
