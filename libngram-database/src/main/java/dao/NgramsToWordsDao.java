package dao;

import dao.entities.NgramToWord;

public interface NgramsToWordsDao {

    NgramToWord findNgramToWord(int id, int wordDbId);

    NgramToWord insertNewNgramsToWords(int ngramId, int wordId);

    void updateNgramToWordCounter(NgramToWord ngramToWord);

    void removeAllEntetiesFromTable();
}
