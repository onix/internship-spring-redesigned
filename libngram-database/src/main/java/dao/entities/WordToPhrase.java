package dao.entities;

public class WordToPhrase {
    public static final String WORD_TO_PHRASE_WORD_ID_COLUMN_NAME = "word_id";
    public static final String WORD_TO_PHRASE_PHRASE_ID_COLUMN_NAME = "phrase_id";
    public static final String WORD_TO_PHRASE_WORDS_IN_PHRASE_COUNT_COLUMN_NAME = "words_in_phrase_count";

    private int wordId;
    private int phraseId;
    private int wordToPhraseCount;

    public WordToPhrase(int wordId, int phraseId, int wordToPhraseCount) {
        this.wordId = wordId;
        this.phraseId = phraseId;
        this.wordToPhraseCount = wordToPhraseCount;
    }

    public int getWordId() {
        return wordId;
    }

    public int getPhraseId() {
        return phraseId;
    }

    public int getWordToPhraseCount() {
        return wordToPhraseCount;
    }

    public void incrementCounter() {
        wordToPhraseCount++;
    }
}
