package dao.entities;

public class NgramToWord {
    public static final String NGRAM_TO_WORD_NGRAM_ID_COLUMN_NAME = "ngram_id";
    public static final String NGRAM_TO_WORD_WORD_ID_COLUMN_NAME = "word_id";
    public static final String NGRAM_TO_WORD_NGRAM_IN_WORD_COUNT_COLUMN_NAME = "ngram_in_word_count";

    private int ngramId;
    private int wordId;
    private int ngramInWordCount;

    public NgramToWord(int ngramId, int wordId, int ngramInWordCount) {
        this.ngramId = ngramId;
        this.wordId = wordId;
        this.ngramInWordCount = ngramInWordCount;
    }

    public int getNgramId() {
        return ngramId;
    }

    public int getWordId() {
        return wordId;
    }

    public int getNgramInWordCount() {
        return ngramInWordCount;
    }

    public void incrementCounter() {
        ngramInWordCount++;
    }
}
