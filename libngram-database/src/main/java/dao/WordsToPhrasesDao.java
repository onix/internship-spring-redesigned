package dao;

import dao.entities.WordToPhrase;

public interface WordsToPhrasesDao {

    WordToPhrase findWordToPhrase(int wordId, int phraseId);

    WordToPhrase insertNewWordToPhrase(int wordId, int phraseId);

    void updateWordToPhraseCounter(WordToPhrase wordToPhrase);

    void removeAllEntetiesFromTable();
}
