package databaseServicesApi;

import libngram.PhraseDataHolder;

public interface DatabaseAdderInterface {
    void clearDatabase();

    void addToDatabase(PhraseDataHolder data);
}
