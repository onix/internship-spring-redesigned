package databaseServicesApi;

/**
 * It's supposed that object gets validated data. Object doesn't do validation.
 */
public class WordForNgramWithCountDto {
    private final String word;
    private final int count;

    public WordForNgramWithCountDto(String word, int count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public int getCount() {
        return count;
    }
}
