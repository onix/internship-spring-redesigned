package databaseServicesApi;

import java.util.List;

public interface WordsDatabaseReaderInterface {
    List<WordForNgramWithCountDto> getListOfWordsWithCounters(WordsListQueryObject wordsListQuery);

    int getAmountOfWordsWithCountersInDatabase(String ngramData, String partOfWord);
}
